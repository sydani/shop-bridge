
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable, MatTableDataSource } from '@angular/material/table'
import { ProductsService } from './products.service';
import { DialogComponent } from './dialog/dialog.component';
import { Product } from './models/product';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(MatTable) table!: MatTable<any>;

  products: any = [];
  displayedColumns: string[] = ['id', 'name', 'price', 'description', 'edit', 'delete'];
  value: string = '';
  
  name:string = "";
  price:string = "";
  description:string = "";
    constructor(private productsService: ProductsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.productsService.getProducts().subscribe((products: any) => {
      this.products = new MatTableDataSource(products);
    });
  }
    
  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.products.filter = filterValue.trim().toLowerCase();
  }

  openDialog(action: string, product: any, index: number) {
    product.action = action;
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: product
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result.data.action == 'Add') {
        this.addProduct(result.data);
      } else if(result.data.action == 'Update') {
        this.updateProduct(result.data, index);
      } else if(result.data.action == 'Delete') {
        this.deleteProduct(result.data, index);
      }
      this.table.renderRows();
    });
  }

  addProduct(data: Product) {
    this.products.data.push(data);
  }

  updateProduct(data: Product, index: number) {
    this.products.data.splice(index, 1, data);
  }

  deleteProduct(data: Product, index: number) {
    this.products.data.splice(index, 1);
  }

resetFilter(): void {
  this.value='';
  this.products.filter = ''
}

}


