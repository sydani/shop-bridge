import { Component, OnInit ,Inject,Optional} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '../models/product';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  
  product: Product;

  constructor(public dialogRef: MatDialogRef<DialogComponent>,
     @Inject(MAT_DIALOG_DATA) public data: Product) { 
      console.log(data);
    this.product = {...data};
  
    }

  ngOnInit(): void {
  }

  handleAction(){
    this.dialogRef.close({data: this.product});
  }

  closeDialog(){
    this.dialogRef.close({data: {...this.product, action: 'Cancel'}});
  }

  

}
